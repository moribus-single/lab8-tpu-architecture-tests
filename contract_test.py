import pytest
from unittest.mock import Mock

from contract import Contract, Signer


@pytest.fixture()
def f_name() -> str:
    return "fake_name"


@pytest.fixture()
def f_tx(
    f_name: str
) -> Mock:
    tx = Mock()
    tx.sign = Mock(return_value=f"sign_{f_name}")

    return tx


def test_contract_send(
    f_tx: Mock
) -> None:
    contr = Contract()
    signed = contr.send(f_tx)

    assert signed == f"ETH21:sign_fake_name"


def test_contract_send_batch(
    f_tx: Mock
) -> None:
    txs = [f_tx, f_tx, f_tx]

    contr = Contract()
    signed = contr.sendBatch(txs)

    assert isinstance(signed, list)
    for tx in signed:
        assert tx == f"ETH21:sign_fake_name"


def test_contract_send_eth(
    f_tx: Mock
) -> None:
    signer = Signer()
    contr = Contract()

    assert signer.getBalance() == 0

    result = contr.sendEth(5, signer)

    assert result is None
    assert signer.getBalance() == 5


def test_raise_error_signer() -> None:
    signer = Signer()

    with pytest.raises(ValueError):
        signer.addETH(5, "fake_sign")



