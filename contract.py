class Signer:
    def __init__(self):
        self.balance = 0

    def addETH(self, value, sign) -> None:
        if sign != hash("CONTRACT"):
            raise ValueError

        self.balance += value

    def getBalance(self) -> int:
        return self.balance


class Contract:
    @staticmethod
    def send(tx) -> str:
        calldata = tx.sign()
        return "ETH21:" + calldata

    @staticmethod
    def sendBatch(txs) -> list:
        output = []

        for tx in txs:
            output.append(
                "ETH21:" + tx.sign()
            )

        return output

    @staticmethod
    def sendEth(value, signer):
        signer.addETH(value, hash("CONTRACT"))



