class Transaction:
    tx = None

    def __init__(
        self,
        sender: str,
        value: int
    ) -> None:
        self.sender = sender
        self.value = value

    def sign(self):
        return hash(
            f"sender={self.sender}?value={self.value}"
        )
